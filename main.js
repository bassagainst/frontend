'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */


const express = require('express');
const bodyParser = require('body-parser');

const path = require('path');
const appx = express();

appx.use(bodyParser.json());
appx.use(bodyParser.urlencoded({extended: false}));

appx.use(express.static(path.join(__dirname)));

// noinspection JSUnresolvedFunction
appx.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});

const server = appx.listen(5475, () => console.log("Démarrage de l'application express..."));

process.on('SIGTERM', shutDown);
process.on('SIGINT', shutDown);

function shutDown() {

    console.log("Extinction de l'application Express...");

    server.close(() => {

        console.log('Suppression de toutes les connections !');
        process.exit(0);
    });

    setTimeout(() => {

        console.error("Suppression du processus. Temps d'attente dépassé !");
        process.exit(1);
    }, 10000);
}


// const path = require('path');
const electron = require('electron');
const {session} = require('electron');

const BrowserWindow = electron.BrowserWindow;
const app = electron.app;

let mainWindow;

function createWindow() {

    mainWindow = new BrowserWindow({
        icon: path.join(__dirname, 'img', 'icon.png'),
        webPreferences: {
            // devTools: false,
            nodeIntegration: false,
            preload: './preload.js'
        },
        width: 900,
        height: 900
    });

    mainWindow.loadURL('http://localhost:5475/');

    mainWindow.focus();

    // mainWindow.setMenu(null);

    mainWindow.on('closed', () => {

        mainWindow = null;
        shutDown();
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {

    if (process.platform !== 'darwin') {

        // noinspection JSUnresolvedVariable
        session.defaultSession.clearStorageData({
            storages: [
                'cookies',
                'localstorage'
            ]
        });

        app.quit();
    }
});

app.on('activate', () => {

    if (mainWindow === null) {

        createWindow();
    }
});
