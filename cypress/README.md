# Madera FrontTest
Pour lancer les tests unitaires du frontend Madera, vous devez installer npm sur votre machine ([installation](https://www.npmjs.com/get-npm)).

## Démarrage
Afin de lancer les premiers tests unitaires, vous devez mettre à jour les dépendances NodeJS :

```bash
npm install
```

Ensuite Cypress a besoin de télécharger ses sources, vous devez entrer la commande :
```bash
./node_modules/cypress/bin/cypress install
```

## Lancement

Pour lancer Cypress en mode graphique, vous pouvez utiliser la commande :

```bash
npx cypress open
```
