'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

describe('gestion_spec', () => {

    beforeEach(() => {
        cy.login();
    });

    it('fournisseur_add', () => {

        cy.visit(Cypress.env('hostname'));

        cy.wait(1000); // slow down

        // Navigation dans les menus
        cy.get(':nth-child(5) > .nav-item > .nav-link > .fa').click();

        cy.wait(1000); // slow down
        cy.get('.entries > [href="/gestion/fournisseurs/liste-fournisseurs"]').click();

        cy.wait(1000); // slow down
        cy.get('.card-header > .btn-default').click();

        cy.wait(1000); // slow down

        // Enregistrement des champs
        cy.get('.m-0 > :nth-child(1) > .form-group > .form-control').type('Cesi Corporation');

        cy.wait(1000); // slow down
        cy.get('#tel').type('03 83 51 83 51');

        cy.wait(1000); // slow down
        cy.get(':nth-child(3) > .form-group > .form-control').type('9');

        cy.wait(1000); // slow down
        cy.get(':nth-child(4) > .form-group > .form-control').type('Rue des violettes');

        cy.wait(1000); // slow down

        // CP/Ville
        cy.get(':nth-child(1) > .typeahead').type('Vandœuvre-lès-Nancy');
        cy.get(':nth-child(2) > .typeahead').type('54500');

        cy.wait(1000); // slow down

        // Ajouter
        cy.get('.btn-success').click();

        cy.wait(3000); // slow down
    });

    it('fournisseur_edit', () =>{

        cy.visit(Cypress.env('hostname') + '/gestion/fournisseurs/liste-fournisseurs');

        cy.wait(1000); // slow down

        cy.get('.form-control').type('Cesi Corporation');

        cy.wait(1000); // slow down

        cy.get('[ng-show="aclDataGrid.read && aclDataGrid.update"]').click();

        cy.wait(1000); // slow down

        cy.get(':nth-child(3) > .form-group > .form-control').clear().type('3');

        cy.wait(1000); // slow down
        cy.get(':nth-child(4) > .form-group > .form-control').clear().type('Rue du Bois de la Champelle');

        cy.wait(1000); // slow down

        cy.get('.btn-success').click();

        cy.wait(3000); // slow down
    });

    it('fournisseur_delete', () => {

        cy.visit(Cypress.env('hostname') + '/gestion/fournisseurs/liste-fournisseurs');

        cy.wait(1000); // slow down

        cy.get('.form-control').type('Cesi Corporation');

        cy.wait(1000); // slow down

        cy.get('[ng-show="aclDataGrid.read || aclDataGrid.update || aclDataGrid.delete"] > .btn-danger').click();

        cy.wait(1000); // slow down

        cy.get('.modal-footer > .btn-danger').click();

        cy.wait(3000); // slow down
    });
});