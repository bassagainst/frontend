'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

describe('profil_spec', () => {

    beforeEach(() => {
        cy.login();
    });

    it('profil', () => {

        cy.visit(Cypress.env('hostname'));

        cy.get('#nav-profil').click();

        cy.wait(1000); // slow down
        cy.get('[href="/profil"]').click();

        cy.wait(1000);

        cy.get(':nth-child(1) > .form-group > .form-control').clear().type('BOLLE');

        cy.wait(1000); // slow down
        cy.get(':nth-child(2) > .form-group > .form-control').clear().type('Jean-Michel');


        cy.wait(1000); // slow down
        cy.get('.btn').click();

        cy.wait(1000); // slow down

        cy.get('#nav-profil').contains('Jean-Michel');

        cy.wait(3000); // slow down
    });
});