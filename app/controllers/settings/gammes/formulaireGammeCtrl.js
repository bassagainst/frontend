'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

// noinspection JSUnusedLocalSymbols
define(['app', 'formulairesCtrl'], function (app, formulairesCtrl) {

    /**
     * @memberof app
     * @ngdoc controllers
     * @name formulaireGammeCtrl
     */
    app.register.controller('formulaireGammeCtrl', [
        '$rootScope', '$scope', '$timeout', '$location', '$controller',
        function ($rootScope, $scope, $timeout, $location, $controller) {

            var typeGammeOptions = [];
            var typeFinitionOptions = [];
            var typeIsolantOptions = [];

            axios.all([
                axios.get('/type-gamme/search'),
                axios.get('/type-finition/search'),
                axios.get('/type-isolant/search')
            ]).then(axios.spread(function (tgRes, tfRes, ticRes) {

                tgRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        typeGammeOptions.push({
                            text: entity.label,
                            value: entity.id
                        });
                    });
                });

                tfRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        typeFinitionOptions.push({
                            text: entity.label,
                            value: entity.id
                        });
                    });
                });

                ticRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        typeIsolantOptions.push({
                            text: entity.label,
                            value: entity.id
                        });
                    });
                });
            }));

            // axios.get('/famille-composant/search')
            //     .then(function (response) {
            //
            //         response.data.data.map(function (entity) {
            //
            //             familleComposantOptions.push({
            //                 text: entity.label,
            //                 value: entity.id
            //             });
            //         });
            //     });

            $scope.itemList = [];
            $scope.itemList.modelNom = 'gamme';
            $scope.itemList.moduleNom = 'settings';
            $scope.itemList.dossierNom = 'gammes';
            $scope.itemList.titreFormAdd = "d'une gamme";
            $scope.itemList.titreFormUpdate = 'de la gamme';
            $scope.itemList.champs = [
                {
                    nom: 'Nom *',
                    field: 'nom',
                    type: 'input_text',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'Qualité huisserie *',
                    field: 'qualite_huisserie',
                    type: 'input_text',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'Description *',
                    field: 'description',
                    type: 'input_textarea',
                    parentDivClass: 'col-12'
                },
                {
                    nom: 'Type de gamme *',
                    field: 'id_type_gamme',
                    type: 'input_select',
                    parentDivClass: 'col-6',
                    options: typeGammeOptions
                },
                {
                    nom: 'Type de finition *',
                    field: 'id_type_finition',
                    type: 'input_select',
                    parentDivClass: 'col-6',
                    options: typeFinitionOptions
                },
                {
                    nom: 'Type d\'isolant *',
                    field: 'id_type_isolant',
                    type: 'input_select',
                    parentDivClass: 'col-12',
                    options: typeIsolantOptions
                }
            ];

            angular.extend(this, $controller('formulairesCtrl', {$scope: $scope}));
        }]);
});
