define(['app','noty','formulairesCtrl'], function (app,noty,formulairesCtrl) {
    app.register.controller('formulaireTvaCtrl', ['$rootScope', '$scope','$timeout','$location','$controller', function ($rootScope, $scope,$timeout,$location,$controller) {
        $scope.itemList = [];
        $scope.itemList.modelNom = 'tva';
        $scope.itemList.moduleNom = 'settings';
        $scope.itemList.dossierNom = 'tva';
        $scope.itemList.titreFormAdd = 'd\'une tva';
        $scope.itemList.titreFormUpdate = 'de la tva';
        $scope.itemList.champs = [
            {'nom' : 'Nom *', 'field' : 'label', 'type' : 'input_text', 'parentDivClass' : 'col-6','champGrpDivClass' : 'form-group', 'champDivClass' : 'form-control'},
            {'nom' : 'Taux (%) *', 'field' : 'taux', 'type' : 'input_float', 'parentDivClass' : 'col-6','champGrpDivClass' : 'form-group', 'champDivClass' : 'form-control'},
            {'nom' : 'Date de début de validité *', 'field' : 'date_debut_validite', 'type' : 'input_date', 'parentDivClass' : 'col-6','champGrpDivClass' : 'form-group', 'champDivClass' : 'form-control datepicker'},
            {'nom' : 'Date de fin de validité *', 'field' : 'date_fin_validite', 'type' : 'input_date', 'parentDivClass' : 'col-6','champGrpDivClass' : 'form-group', 'champDivClass' : 'form-control datepicker'},
        ];
        angular.extend(this, $controller('formulairesCtrl', {$scope: $scope}));
    }])
});
