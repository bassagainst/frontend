'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

define(['app'], function (app) {

    app.register.controller('homeCtrl', [
        '$rootScope', '$scope', '$timeout', '$location', '$controller',
        function ($rootScope, $scope, $timeout, $location, $controller) {

            $scope.itemList = [];
            $scope.itemList.modelNom = 'historique';
            $scope.itemList.moduleNom = 'historique';
            $scope.itemList.cols = [
                {
                    label: 'Type',
                    champ: 'crud'
                },
                {
                    label: 'Utilisateur',
                    champ: 'utilisateur_nom',
                    trust: true
                },
                {
                    label: 'Objet',
                    champ: 'table'
                },
                {
                    label: 'Date',
                    champ: 'date'
                }
            ];

            //Initialisation variables du scope
            $scope.filtre = "";
            $scope.order = "";
            $scope.loading = false;
            $scope.current = [];
            $scope.stats = "";
            $scope.aclDataGrid = [];

            $scope.itemList.current = 0;

            index($scope.itemList.current);

            axios.get('/statistique/dashboard')
                .then(function (response) {

                    $scope.$apply(function () {

                        $scope.stats = response.data.data;
                    });
                });

            // Récupération de la liste
            function index(page = 0, filtre = null, order = null) {

                var data = new FormData();

                if (filtre != null && filtre !== '') data.append('query', filtre);
                if (order != null) data.append('order', order);

                data.append('offset', 8 * page);

                $scope.loading = true;

                axios.post('/' + $scope.itemList.modelNom + '/search', data)
                    .then(function (response) {
                        $scope.$apply(function () {

                            $scope.loading = false;

                            if (response.data) {

                                if (response.data.data) $scope.itemList.response = response.data.data;
                                if (response.data.acl) $scope.aclDataGrid = response.data.acl;
                                if (response.data.total) $scope.itemList.total = response.data.total;
                                if (response.data.total) $scope.itemList.pages = Math.ceil(response.data.total / 8);
                            }
                        });
                    })
                    .catch(function (error) {

                        $scope.$apply(function () {

                            $scope.loading = false;
                            $scope.error = error.message;
                        });
                    });
            };

            $rootScope.refreshTable = function (page = 0, filtre = null, order = null) {

                if (order != null && order + ' DESC' === $scope.order) {

                    order = null;

                    $timeout(function () {

                        $scope.order = order;
                    });

                } else if (order != null && order === $scope.order && !order.includes(' DESC')) {

                    order = order + ' DESC';

                    $timeout(function () {

                        $scope.order = order;
                    });

                } else if (order != null) {

                    $timeout(function () {

                        $scope.order = order;
                    });

                } else if ($scope.order != null) {

                    order = $scope.order;
                }

                if (page !== $scope.itemList.current) {

                    $timeout(function () {

                        $scope.itemList.current = page;
                    });
                }

                if (filtre !== $scope.filtre) {

                    $timeout(function () {

                        $scope.filtre = filtre;
                    });
                }

                index(page, filtre, order);
            };

            /**
             * Redirige vers la page en question
             * @param item
             */
            // $scope.redirectToLink = function (item) {
            //     var id_table = item.id_table;
            //     var table = item.table;
            //     var arrayRoute = {
            //         Gamme: 'settings/gammes',
            //         Type_Gamme: 'settings/typegammes',
            //         Type_Finition: 'settings/typefinitions',
            //         Type_Isolant: 'settings/typeisolants',
            //         Module: 'settings/modules',
            //         Nature_Module: 'settings/naturemodules',
            //         Composant: 'settings/composants',
            //         Famille_Composant: 'settings/famillecomposants',
            //         Etat_Devis: 'settings/etatdevis',
            //         Tva: 'settings/tva',
            //         Projet: 'conception/projets',
            //         Devis: 'conception/devis',
            //         Utilisateur: 'admin/utilisateurs',
            //         Client : 'gestion/clients',
            //         Fournisseur : 'gestion/fournisseurs'
            //     };
            //     $location.path("/"+ arrayRoute[table] +"/formulaire/" + id_table);
            // }
        }]);
});
