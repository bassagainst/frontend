'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

// noinspection JSUnusedLocalSymbols
define(['app', 'noty'], function (app, noty) {

    /**
     * @memberof app
     * @ngdoc controllers
     * @name formulaireAclsCtrl
     */
    app.register.controller('formulaireAclsCtrl', [
        '$rootScope', '$scope', '$timeout', '$location', '$controller',
        function ($rootScope, $scope, $timeout, $location, $controller) {

            $scope.opts = [];
            $scope.liste = [];
            $scope.roles = '';
            $scope.itemList = [];
            $scope.isEdit = false;
            $scope.loading = false;
            $scope.roleSelect = '';
            $scope.accessSelect = '';
            $scope.resourceSelect = '';
            $scope.itemList.cols = [
                {
                    label: 'Ressource',
                    champ: 'resources_name'
                },
                {
                    label: 'Accès',
                    champ: 'access_name'
                },
                {
                    label: 'Autorisation',
                    champ: 'allowed',
                    style: {
                        width: '130px'
                    }
                }
            ];

            axios.get('/acl/roles')
                .then(function (response) {

                    $scope.$apply(function () {

                        $scope.roles = response.data.data;
                    });
                })
                .catch(function (error) {
                    $scope.$apply(function () {
                        noty.showNotif('L\'élément demandé n\'existe pas', 'error');
                    });
                });

            var resourcesOptions = [];

            axios.get('/acl/resources')
                .then(function (response) {

                    response.data.data.forEach(function (i) {

                        resourcesOptions.push({
                            text: i.name,
                            value: i.name
                        });
                    });
                })
                .catch(function (error) {
                    $scope.$apply(function () {
                        noty.showNotif('L\'élément demandé n\'existe pas', 'error');
                    });
                });

            $scope.itemList.champsModalRole = [
                {
                    nom: 'Nom *',
                    field: 'name',
                    type: 'input_text',
                    parentDivClass: 'col-6',
                    edit: {
                        disabled: true
                    }
                },
                {
                    nom: 'Description *',
                    field: 'description',
                    type: 'input_textarea',
                    parentDivClass: 'col-6'
                }
            ];

            $scope.itemList.champsModalAccess = [
                {
                    type: 'input_select_double',
                    parentDivClass: 'col-6',
                    nom: {
                        a: 'Ressource *',
                        b: 'Accès *'
                    },
                    field: {
                        a: 'resource',
                        b: 'access'
                    },
                    // empty: {
                    //     a: false,
                    //     b: false
                    // },
                    options: resourcesOptions
                },
                {
                    nom: 'Autorisation *',
                    field: 'grant',
                    type: 'input_select',
                    parentDivClass: 'col-6',
                    empty: false,
                    options: [
                        {
                            text: 'Oui',
                            value: 1
                        },
                        {
                            text: 'Non',
                            value: 0
                        }
                    ]
                }
            ];

            $scope.loadAccesses = function (resource) {

                var accesses = [];

                axios.get('/acl/accesses/' + resource)
                    .then(function (response) {

                        response.data.data.forEach(function (i) {

                            accesses.push({
                                text: i.access,
                                value: i.access
                            });
                        });

                        $scope.$apply(function () {

                            $scope.opts = accesses;
                        });
                    })
            };

            $scope.selectResourceAccess = function (access_name, resources_name) {
                $scope.accessSelect = access_name;
                $scope.resourceSelect = resources_name;
            };

            $scope.accessList = function (id = null) {

                if (id != null && id) {

                    $scope.loading = true;

                    var access = new DOMParser().parseFromString(id, "text/html");

                    axios.get(encodeURI('acl/access/' + access.documentElement.textContent))
                        .then(function (response) {

                            $scope.$apply(function () {

                                $scope.loading = false;
                                $scope.liste = response.data.data;
                            });
                        })
                        .catch(function (error) {

                            $scope.$apply(function () {

                                $scope.loading = false;
                                noty.showNotif('L\'élément demandé n\'existe pas', 'error');
                            });
                        });
                }
            };

            $scope.displayAddAccess = function () {

                $scope.liste = {grant: 1, resource: '*', access: '*'};

                $('#access-modal').modal('show');
            };

            $scope.delAccess = function (role = null, resource = null, access = null) {

                if (role != null && resource != null && access != null && role && resource && access) {

                    axios.delete('/acl/access/' + role + '/' + resource + '/' + access)
                        .then(function (response) {

                            $scope.$apply(function () {

                                $scope.liste = '';
                                $scope.accessList(role);
                            });
                        })
                        .catch(function (error) {

                            $scope.$apply(function () {

                                noty.showNotif("L'accès n'est pas supprimable !", 'error');
                            });
                        });
                }
            };

            $scope.delRole = function (id = null) {

                if (id != null && id) {

                    $scope.loading = true;

                    var access = new DOMParser().parseFromString(id, "text/html");

                    axios.delete(encodeURI('/acl/roles/' + access.documentElement.textContent))
                        .then(function (response) {

                            $scope.$apply(function () {

                                $scope.loading = false;
                                $scope.roles = response.data.list;
                                $scope.roleSelect = '';
                                $scope.liste = '';
                            });
                        })
                        .catch(function (error) {

                            $scope.$apply(function () {

                                $scope.loading = false;
                                noty.showNotif("Le rôle est utilisé, vous ne pouvez pas le supprimer !", 'error');
                            });
                        });
                }
            };

            $scope.addAccess = function (id = null) {

                if (id != null && id) {

                    var data = new FormData();

                    $scope.itemList.champsModalAccess.forEach(function (champ) {

                        if (champ['type'] === "input_select_double") {

                            data.append(champ['field']['a'], $scope.liste[champ['field']['a']] ? $scope.liste[champ['field']['a']] : '');
                            data.append(champ['field']['b'], $scope.liste[champ['field']['b']] ? $scope.liste[champ['field']['b']] : '');

                        } else {

                            data.append(champ['field'], $scope.liste[champ['field']] ? $scope.liste[champ['field']] : '');
                        }
                    });

                    axios.post('/acl/access/' + $scope.roleSelect, data)
                        .then(function (response) {

                            $scope.$apply(function () {

                                $scope.accessList($scope.roleSelect);
                            });
                        })
                        .catch(function (error) {
                            $scope.$apply(function () {
                                noty.showNotif('L\'élément demandé n\'existe pas', 'error');
                            });
                        });
                }
            };

            $scope.addRole = function () {

                var data = new FormData();

                $scope.itemList.champsModalRole.forEach(function (champ) {

                    data.append(champ['field'], $scope.liste[champ['field']] ? $scope.liste[champ['field']] : '');
                });

                axios.post('/acl/roles', data)
                    .then(function (response) {

                        $scope.$apply(function () {

                            $scope.roles = response.data.data.list;
                            $scope.roleSelect = response.data.data.name;

                            $scope.accessList(response.data.data.name);
                        });
                    })
                    .catch(function (error) {
                        $scope.$apply(function () {
                            noty.showNotif('L\'élément demandé n\'existe pas', 'error');
                        });
                    });
            };

            $scope.modalRoleDisplay = function (id = null) {

                $scope.isEdit = false;

                if (id !== null) {

                    $scope.isEdit = true;

                    var access = new DOMParser().parseFromString(id, "text/html");

                    axios.get(encodeURI('/acl/roles/' + access.documentElement.textContent))
                        .then(function (response) {

                            $('#roles-modal').modal('show');

                            $scope.$apply(function () {

                                $scope.liste = response.data.data;
                            });
                        })
                        .catch(function (error) {
                            $scope.$apply(function () {
                                noty.showNotif('L\'élément demandé n\'existe pas', 'error');
                            });
                        });

                } else {

                    $('#roles-modal').modal('show');

                    $scope.itemList.champsModalRole.forEach(function (champ) {

                        $scope.liste[champ['field']] = '';
                    });
                }
            }
        }]);
});
