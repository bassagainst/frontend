'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

define(['app','noty'], function (app,noty) {

    app.register.controller('profilCtrl', [
        '$rootScope', '$scope', '$timeout', '$location', '$controller',
        function ($rootScope, $scope, $timeout, $location, $controller) {

            $scope.saveLoading = false;

            axios.get('/utilisateur/profil')
                .then(function (response) {
                    $scope.$apply(function () {
                        $scope.liste = response.data.data;
                        $scope.titre = 'Modification du profil';
                    });
                })
                .catch(function (error) {
                    $scope.$apply(function () {
                        noty.showNotif('L\'élément demandé n\'existe pas', 'error');
                    });
                });

            $scope.update = function () {
                $scope.saveLoading = true;

                var data = new FormData();

                data.append("nom",$scope.liste.nom);
                data.append("prenom",$scope.liste.prenom);
                data.append("tel",$scope.liste.tel);
                data.append("mdp",$scope.liste.mdp ? $scope.liste.mdp : '');
                data.append("conf_mdp",$scope.liste.conf_mdp ? $scope.liste.mdp : '');

                axios.post('/utilisateur/profil',data)
                    .then(function (response) {
                        $scope.$apply(function () {
                            noty.showNotif('Profil modifié', 'success');
                            $scope.saveLoading = false;
                            $location.path("/");
                        });

                        $rootScope.reconnect();
                    })
                    .catch(function (error) {
                        $scope.$apply(function () {
                            noty.showNotif('L\'élément demandé n\'existe pas', 'error');
                            $scope.saveLoading = false;
                            if (error.response && error.response.data && error.response.data.messages)
                                $scope.errors = error.response.data.messages;
                        });
                    });
            }
        }]);
});
