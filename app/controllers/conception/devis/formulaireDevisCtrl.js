'use strict';

/*
 +----------------------------------------------------------------------------------+
 | BassAgainst - Madera                                                             |
 | An open source application for Madera Project                                    |
 +----------------------------------------------------------------------------------+
 | Copyright (c) BassAgainst (https://gitlab.com/bassagainst/)                      |
 +----------------------------------------------------------------------------------+
 | All rights reserved.                                                             |
 |                                                                                  |
 | Redistribution and use in source and binary forms, with or without               |
 | modification, are permitted provided that the following conditions are met:      |
 |  * Redistributions of source code must retain the above copyright                |
 | notice, this list of conditions and the following disclaimer.                    |
 |  * Redistributions in binary form must reproduce the above copyright             |
 | notice, this list of conditions and the following disclaimer in the              |
 | documentation and/or other materials provided with the distribution.             |
 |  * Neither the name of the BassAgainst nor the                                   |
 | names of its contributors may be used to endorse or promote products             |
 | derived from this software without specific prior written permission.            |
 |                                                                                  |
 | THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"      |
 | AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE        |
 | IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE       |
 | ARE DISCLAIMED. IN NO EVENT SHALL BASSAGAINST BE LIABLE FOR ANY DIRECT,          |
 | INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES               |
 | (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;     |
 | LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND      |
 | ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT       |
 | (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF         |
 | THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                |
 +----------------------------------------------------------------------------------+
 */

// noinspection JSUnusedLocalSymbols
define(['app', 'config', 'formulairesCtrl'], function (app, config, formulairesCtrl) {

    /**
     * @memberof app
     * @ngdoc controllers
     * @name formulaireDevisCtrl
     */
    app.register.controller('formulaireDevisCtrl', [
        '$rootScope', '$scope', '$timeout', '$location', '$controller',
        function ($rootScope, $scope, $timeout, $location, $controller) {

            var ProjetOptions = [];
            var EtatOptions = [];
            var GammeOptions = [];
            var NatureModuleOptions = [];
            var ComposantOptions = [];
            var ModeleDevisOptions = [];

            var data = new FormData();
            data.append('id', $scope.id);

            $timeout(function () {

                var sessionToken = jwt_decode(localStorage.getItem("refreshToken"));

                $scope.devisLink = config.Api.baseURL + 'print/devis/'
                    + $rootScope.currentRoute.params['id'] + '?authorization=' + sessionToken.payload.token;
            });

            axios.all([
                axios.get('/projet/search'),
                axios.get('/etat-devis/search'),
                axios.get('/gamme/search'),
                axios.get('/nature-module/search'),
                axios.get('/composant/search'),
                axios.post('/module/search',data),
            ]).then(axios.spread(function (cRes, uRes, gRes, nmRes, cpRes,mRes) {

                cRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        ProjetOptions.push({
                            text: entity.nom + ' (' + entity.reference + ')',
                            value: entity.id
                        });
                    });
                });

                uRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        EtatOptions.push({
                            text: entity.label,
                            value: entity.id
                        });
                    });
                });

                gRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        GammeOptions.push({
                            text: entity.nom,
                            value: entity.id
                        });
                    });
                });

                nmRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        NatureModuleOptions.push({
                            text: entity.nom,
                            value: entity.id
                        });
                    });
                });

                cpRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        ComposantOptions.push({
                            text: entity.nom,
                            value: entity.id
                        });
                    });
                });

                mRes.data.data.map(function (entity) {

                    $scope.$apply(function () {
                        ModeleDevisOptions.push({
                            text: entity.nom,
                            value: entity.id
                        });
                    });
                });
            }));

            $scope.itemList = [];
            $scope.itemList.modelNom = 'devis';
            $scope.itemList.moduleNom = 'conception';
            $scope.itemList.dossierNom = 'devis';
            $scope.itemList.titreFormAdd = "d'un devis";
            $scope.itemList.titreFormUpdate = 'Modification du devis';
            $scope.itemList.modelCibleNv1 = 'module';
            $scope.itemList.idElem = 'id_devis';
            $scope.itemList.idElemNv2 = 'id_module';
            $scope.itemList.param2 = 'isModele';
            $scope.itemList.typeGet = 'POST';
            $scope.itemList.modelGetModalEdit = 'devis/module';
            $scope.itemList.modelModalSave = 'module/composant';
            $scope.itemList.modelCible = 'module';
            $scope.itemList.haveTab = true;
            $scope.itemList.showNavTab = true;
            $scope.itemList.champs = [
                {
                    nom: 'Projet *',
                    field: 'id_projet',
                    type: 'input_select',
                    parentDivClass: 'col-12',
                    options: ProjetOptions,
                    edit: {
                        disabled: true
                    }
                },
                {
                    nom: 'État du devis *',
                    field: 'id_etat',
                    type: 'input_select',
                    parentDivClass: 'col-6',
                    options: EtatOptions
                },
                {
                    nom: 'Date de fin de validité *',
                    field: 'date_fin_validite',
                    type: 'input_date',
                    parentDivClass: 'col-6'
                },
                {
                    nom: 'Note publique *',
                    field: 'note_publique',
                    type: 'input_textarea',
                    parentDivClass: 'col-12'
                },
                {
                    nom: 'Note privée *',
                    field: 'note_privee',
                    type: 'input_textarea',
                    parentDivClass: 'col-12'
                }
            ];
            $scope.itemList.champs2 = [
                {
                    nom: 'Modules du devis',
                    options : ModeleDevisOptions,
                    field:'object',
                    type: 'input_grid',
                    parentDivClass: 'col-12',
                    modelCible: 'module',
                    isHuge: true,
                    modelSave:'devis/module',
                    cols: [
                        {
                            label: 'Module',
                            champ: 'nom'
                        },
                        {
                            label: 'Gamme',
                            champ: 'gamme_nom'
                        },
                        {
                            label: 'Marge',
                            champ: 'marge'
                        },
                        {
                            label: 'Nature',
                            champ: 'naturemodule_nom'
                        }
                    ],
                    champsModal: [
                        {
                            nom: 'Nom *',
                            field: 'nom',
                            type: 'input_modal_text',
                            parentDivClass: 'col-6'
                        },
                        {
                            nom: 'Marge *',
                            field: 'marge',
                            type: 'input_modal_float',
                            parentDivClass: 'col-6'
                        },
                        {
                            nom: 'Gamme *',
                            field: 'id_gamme',
                            type: 'input_modal_select',
                            parentDivClass: 'col-6',
                            options: GammeOptions
                        },
                        {
                            nom: 'Nature de module *',
                            field: 'id_nature',
                            type: 'input_modal_select',
                            parentDivClass: 'col-6',
                            options: NatureModuleOptions
                        },
                        {
                            nom: 'Description *',
                            field: 'description',
                            type: 'input_modal_textarea',
                            parentDivClass: 'col-12'
                        },
                        {
                            nom: 'Composants du module',
                            field:'object',
                            type: 'input_modal_grid',
                            parentDivClass: 'col-12',
                            modelCible: 'composant',
                            modelSave:'module/composant',
                            cols: [
                                {
                                    label: 'Nom',
                                    champ: 'nom'
                                },
                                {
                                    label: 'Quantité',
                                    champ: 'quantite'
                                },
                                {
                                    label: 'Prix unitaire',
                                    champ: 'prix_ht'
                                },
                                {
                                    label: 'Marge',
                                    champ: 'marge'
                                }
                            ],
                            champsModal: [
                                {
                                    nom: 'Nom *',
                                    field: 'id_composant',
                                    type: 'input_modalNv2_select',
                                    parentDivClass: 'col-12',
                                    options: ComposantOptions
                                },
                                {
                                    nom: 'Quantité *',
                                    field: 'quantite',
                                    type: 'input_modalNv2_number',
                                    parentDivClass: 'col-12'
                                }
                            ]
                        }
                    ]
                }
            ];

            angular.extend(this, $controller('formulairesCtrl', {$scope: $scope}));
        }]);
});
