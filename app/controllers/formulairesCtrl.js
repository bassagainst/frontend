define(['app', 'noty'], function (app, noty) {
    app.register.controller('formulairesCtrl', ['$rootScope', '$scope', '$timeout', '$location', function ($rootScope, $scope, $timeout, $location) {

        //Initialisation variables
        $scope.titre = 'Ajout ' + $scope.itemList.titreFormAdd;
        $scope.isEdit = false;
        $scope.saveLoading = false;
        $scope.liste = [];
        $scope.listeModal = [];
        $scope.listeModalNv2 = [];
        $scope.errors = [];
        $scope.aclFormRead = [];
        $scope.current = [];
        $scope.showTabNv2 = false;
        var id = $rootScope.currentRoute.params['id'];

        // //Add ou edit
        if (id !== undefined) {
            //Récupération des infos
            axios.get('/' + $scope.itemList.modelNom + '/' + id)
                .then(function (response) {
                    $scope.$apply(function () {
                        $scope.liste = response.data.data;
                        if (response.data.acl) $scope.aclFormRead = response.data.acl;
                        $scope.id = id;
                        $scope.isEdit = true;

                        //Si le champ label s'appelle autrement
                        if ($scope.liste['label'] === undefined) {  //si non label
                            if ($scope.liste['nom'] === undefined) {    //si non nom
                                $scope.liste['label'] = $scope.liste['reference'];  //alors reference
                            } else {
                                if ($scope.liste['prenom'] !== undefined) {
                                    $scope.liste['label'] = $scope.liste['nom'] + ' ' + $scope.liste['prenom'];
                                } else {
                                    $scope.liste['label'] = $scope.liste['nom'];
                                }
                            }
                        }
                        $scope.titre = $scope.itemList.titreFormUpdate + ' | ' + $scope.liste['label'];

                        if ($scope.itemList.haveTab !== undefined)
                            getElemForTab();

                        //En fonction des types
                        $scope.itemList.champs.forEach(function (champ) {
                            if (champ['type'] === "input_number")
                                $scope.liste[champ['field']] = parseInt($scope.liste[champ['field']]);
                            if (champ['type'] === "input_float")
                                $scope.liste[champ['field']] = parseFloat($scope.liste[champ['field']]);
                            if (champ['type'] === "input_cpville") {
                                $scope.liste[champ['field']['cp']] = $scope.liste[champ['field']['cp']];
                                $scope.liste[champ['field']['ville']] = $scope.liste[champ['field']['ville']];
                            }
                        });
                    });
                })
                .catch(function (error) {
                    $scope.$apply(function () {
                        noty.showNotif('L\'élément demandé n\'existe pas', 'error');
                    });
                });
        }

        /**
         * Création / Modification d'un élément
         * @param id
         */
        $scope.update = function (id = null) {
            var data = new FormData();

            //Récupération des paramètres
            $scope.itemList.champs.forEach(function (champ) {

                if (typeof champ.field === 'object') {

                    Object.keys(champ.field).map(function (objectKey, index) {

                        var value = champ.field[objectKey];
                        data.append(value, $scope.liste[value] ? $scope.liste[value] : '');
                    });

                } else {

                    data.append(champ['field'], $scope.liste[champ['field']] ? $scope.liste[champ['field']] : '');
                }
            });

            $scope.saveLoading = true;

            //Enregistrement
            if (id == null) //Ajout
            {
                axios.post('/' + $scope.itemList.modelNom, data)
                    .then(function () {
                        $scope.saveLoading = false;
                        $timeout(function () {
                            noty.showNotif('Élément créé', 'success');
                            $location.path("/" + $scope.itemList.moduleNom + "/" + $scope.itemList.dossierNom + "/liste-" + $scope.itemList.dossierNom);
                        });
                    })
                    .catch(function (error) {
                        $scope.saveLoading = false;
                        $scope.$apply(function () {
                            noty.showNotif('L\'élément n\'a pas pu être créé', 'error');

                            if (error.response && error.response.data && error.response.data.messages)
                                $scope.errors = error.response.data.messages;
                        });
                    });
            }
            else    //Modif
            {
                axios.post('/' + $scope.itemList.modelNom + '/' + id, data)
                    .then(function () {
                        $scope.saveLoading = false;
                        $timeout(function () {
                            noty.showNotif('Élément modifié', 'success');
                            $location.path("/" + $scope.itemList.moduleNom + "/" + $scope.itemList.dossierNom + "/liste-" + $scope.itemList.dossierNom);
                        });
                    })
                    .catch(function (error) {
                        $scope.saveLoading = false;
                        $scope.$apply(function () {

                            if (error.response && error.response.data && error.response.data.messages)
                                noty.showNotif('L\'élément n\'a pas pu être modifié : ' + error.response.data.messages.join(' | '), 'error');
                        });
                    });
            }
        };

        /**
         * Suppression d'un élément
         */
        $scope.delete = function () {
            axios.delete('/' + $scope.itemList.modelNom + '/' + id)
                .then(function (response) {
                    $timeout(function () {
                        noty.showNotif('Élément archivé', 'success');
                        $location.path("/" + $scope.itemList.moduleNom + "/" + $scope.itemList.dossierNom + "/liste-" + $scope.itemList.dossierNom);
                    });
                })
                .catch(function (error) {
                    $scope.$apply(function () {
                        noty.showNotif('L\'élément n\'a pas pu être archivé', 'error');
                    });
                });
        };

        /**
         * Ouvre une modale générique de niveau 1
         * @param model
         * @param isHuge
         * @param action
         * @param id
         */
        $scope.openGenericModalNv1 = function (model, isHuge = undefined, action = 'add', id) {
            $scope.listeModal = [];
            $scope.itemList.responseModal = [];
            $scope.showTabNv2 = false;
            if (isHuge !== undefined) {
                $('#modalAjHugeNv1' + model + '').modal('show');
                if (action === 'edit') {
                    if (id !== undefined) {
                        $scope.showTabNv2 = true;
                        $scope.actionModal = 'Modifier';
                        getElemForEditModal(id);
                    }
                } else {
                    $scope.actionModal = 'Ajouter';
                }
            } else {
                $('.modalAjNv1' + model + '').modal('show');
            }
        };

        /**
         * Ouvre une modale générique de niveau 2
         * @param model
         */
        $scope.openGenericModalNv2 = function (model) {
            $('.modalAjNv2' + model + '').modal('show');
        };

        /**
         * Ferme une modale de niveau 1
         * @param model
         * @param isHuge
         */
        $scope.closeGenericModalNv1 = function (model, isHuge = undefined) {
            if (isHuge !== undefined) {
                $('#modalAjHugeNv1' + model + '').modal('hide');
            } else {
                $('#modalAjNv1' + model + '').modal('hide');
            }
        };

        /**
         * Ferme une modale de niveau 2
         * @param model
         */
        $scope.closeGenericModalNv2 = function (model) {
            $('.modalAjNv2' + model + '').modal('hide');
        };

        /**
         * Ouvre une modale de suppression d'item dans une modal grid
         * @param id
         * @param model
         */
        $scope.openModal = function (id, model) {
            $('#modalSu' + model + '').modal('show');
            $scope.current.id = id;
        };

        /**
         * Récupère les éléments d'un élément parent
         * @param inModal
         * @param idElem
         */
        function getElemForTab(inModal = null, idElem = null) {
            if ($scope.itemList.typeGet === 'POST') {
                getElemForTabPost();
            } else {
                var last_id = $scope.id;
                var id = -1;
                if (inModal)
                    id = idElem;
                else
                    id = $scope.id;
                axios.get('/' + $scope.itemList.modelModalSave + '/' + id)
                    .then(function (response) {
                        $scope.$apply(function () {
                            if (response.data) {
                                if (response.data.data) {
                                    if (inModal != null)
                                        $scope.itemList.responseModal = response.data.data;
                                    else
                                        $scope.itemList.response = response.data.data;
                                    $scope.itemList.typeGet = 'POST';
                                    $scope.id = last_id;
                                }
                            }
                        });
                    })
                    .catch(function (error) {
                        $scope.error = error.message;
                    });
            }
        };

        /**
         * Récupère les éléments d'un élément parent (en POST)
         */
        function getElemForTabPost() {
            var data = new FormData();

            data.append($scope.itemList.idElem, $scope.id);
            if ($scope.itemList.param2 !== undefined)
                data.append($scope.itemList.param2, 0);

            axios.post('/' + $scope.itemList.modelCibleNv1 + '/search', data)
                .then(function (response) {
                    $scope.$apply(function () {
                        if (response.data) {
                            if (response.data.data) {
                                $scope.itemList.response = response.data.data;
                            }
                        }
                    });
                })
                .catch(function (error) {
                    $scope.error = error.message;
                });
        };

        /**
         * Récupérer les données du formulaire d'une modale
         * @param id
         */
        function getElemForEditModal(id) {
            axios.get('/' + $scope.itemList.modelGetModalEdit + '/' + id)
                .then(function (response) {
                    $scope.$apply(function () {
                        if (response.data.data) {
                            $scope.listeModal = response.data.data;
                            $scope.idNv2 = $scope.listeModal.id;
                            $scope.itemList.typeGet = null;
                            getElemForTab(true, $scope.idNv2);
                        }
                    });
                })
                .catch(function (error) {
                    $scope.error = error.message;
                });
        };

        /**
         * Ajoute un élément à un tableau déjà présent dans un formulaire
         * @param modelSave
         * @param isMultiModal
         * @param becauseImAChacal Parce qu'il est tard, mon cerveau est en vrac et que je ne trouve pas de nom à ce paramètre, sert à faire l'ajout de l'élément de la modale nv2
         * @param modelCible
         * @param isEdit
         */
        $scope.addElemToTab = function (modelSave, isMultiModal = false, becauseImAChacal = null, modelCible = null, isEdit = false) {
            var data = new FormData();

            var champs = [];
            if (isMultiModal && becauseImAChacal) {
                var count = $scope.itemList.champs2[0].champsModal.length - 1;
                champs = $scope.itemList.champs2[0].champsModal[count];
            }
            if (isMultiModal && (becauseImAChacal == null))
                champs = $scope.itemList.champs2;
            if (!isMultiModal)
                champs = $scope.itemList.champs;


            if (!isMultiModal || !becauseImAChacal) {
                //Récupération des paramètres de la modale
                champs.forEach(function (champ) {
                    if (typeof champ.field === 'object' && (champ.type === 'input_grid' || champ.type === 'input_modal_grid')) {
                        champ.champsModal.forEach(function (champModal) {
                            Object.keys(champModal.field).map(function (objectKey) {
                                var value = champModal.field[objectKey];
                                data.append(value, $scope.listeModal[value] ? $scope.listeModal[value] : '');
                            });
                        });

                    } else {
                        if (champ.type === 'input_grid' || champ.type === 'input_modal_grid') {
                            champ.champsModal.forEach(function (champModal) {
                                data.append(champModal['field'], $scope.listeModal[champModal['field']] ? $scope.listeModal[champModal['field']] : '');
                            });
                        }
                    }
                });
            } else {
                //Récupération des paramètres de la modale Nv2

                if (typeof champs.field === 'object' && (champs.type === 'input_grid' || champs.type === 'input_modal_grid')) {
                    $scope.itemList.champs.champsModal.forEach(function (champModal) {
                        Object.keys(champModal.field).map(function (objectKey) {
                            var value = champModal.field[objectKey];
                            data.append(value, $scope.listeModalNv2[value] ? $scope.listeModalNv2[value] : '');
                        });
                    });

                } else {
                    if (champs.type === 'input_grid' || champs.type === 'input_modal_grid') {
                        champs.champsModal.forEach(function (champModal) {
                            data.append(champModal['field'], $scope.listeModalNv2[champModal['field']] ? $scope.listeModalNv2[champModal['field']] : '');
                        });
                    }
                }

            }


            if ($scope.idNv2 === undefined || !becauseImAChacal) {
                data.append($scope.itemList.idElem, $scope.id);
            } else {
                data.append($scope.itemList.idElemNv2, String($scope.idNv2));
            }


            if (isEdit !== false) {
                //Edition
                axios.post('/' + modelSave + '/' + $scope.idNv2, data)
                    .then(function () {
                        $timeout(function () {
                            noty.showNotif('Élément modifié', 'success');
                            $scope.itemList.typeGet = 'GET';
                            // if (isMultiModal && becauseImAChacal && ($scope.idNv2 !== undefined)) {
                            //     getElemForTab(true, $scope.idNv2);
                            // } else {
                            //     $scope.itemList.typeGet = 'POST';
                            //     getElemForTab();
                            // }
                            if (isMultiModal && becauseImAChacal && ($scope.idNv2 !== undefined)) {
                                getElemForTab(true, $scope.idNv2);
                            }
                            if (isMultiModal && !becauseImAChacal && ($scope.idNv2 !== undefined)) {
                                $scope.itemList.typeGet = 'POST';
                                getElemForTab();
                            }
                            if (!isMultiModal) {
                                getElemForTab();
                            }
                            $scope.closeGenericModalNv2(modelCible);
                        });
                    })
                    .catch(function (error) {
                        $scope.$apply(function () {
                            noty.showNotif('L\'élément n\'a pas pu être ajouté', 'error');

                            if (error.response && error.response.data && error.response.data.messages)
                                $scope.errors = error.response.data.messages;
                        });
                    });
            } else {

                //Ajout
                axios.post('/' + modelSave, data)
                    .then(function () {
                        $timeout(function () {
                            noty.showNotif('Élément ajouté', 'success');
                            $scope.itemList.typeGet = 'GET';
                            if (isMultiModal && becauseImAChacal && ($scope.idNv2 !== undefined)) {
                                getElemForTab(true, $scope.idNv2);
                            }
                            if (isMultiModal && !becauseImAChacal && ($scope.idNv2 !== undefined)) {
                                $scope.itemList.typeGet = 'POST';
                                getElemForTab();
                            }
                            if (!isMultiModal) {
                                getElemForTab();
                            }
                            $scope.closeGenericModalNv2(modelCible);
                        });
                    })
                    .catch(function (error) {
                        $scope.$apply(function () {
                            noty.showNotif('L\'élément n\'a pas pu être ajouté', 'error');

                            if (error.response && error.response.data && error.response.data.messages)
                                $scope.errors = error.response.data.messages;
                        });
                    });
            }
        };

        /**
         * Suppression d'un élément de modale
         */
        $scope.deleteElemFromTab = function (id, model, nv2 = false) {
            axios.delete('/' + model + '/' + id)
                .then(function (response) {
                    $timeout(function () {
                        // noty.showNotif('Élément supprimé', 'success');
                        if (nv2) {
                            $scope.itemList.typeGet = 'GET';
                            getElemForTab(true, $scope.idNv2);
                        }
                        if (!nv2 && $scope.itemList.isGet !== true) {
                            getElemForTab();
                        }
                        if (nv2 && $scope.itemList.isGet === true){
                            $scope.itemList.typeGet = 'GET';
                            getElemForTab(true, $scope.idNv2);
                        }
                        if ($scope.itemList.isGet === true) {
                            $scope.itemList.typeGet = 'GET';
                            getElemForTab();
                        }

                    });
                })
                .catch(function (error) {
                    $scope.$apply(function () {
                        noty.showNotif('L\'élément n\'a pas pu être supprimé', 'error');
                    });
                });
        };

        /**
         * Charge un élément dans le formulaire de la modale
         * @param id
         */
        $scope.loadElemToModal = function (id) {
            getElemForEditModal(id);
        }
    }])
});
